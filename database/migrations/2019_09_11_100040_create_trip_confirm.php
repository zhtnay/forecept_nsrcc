<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripConfirm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trip_code');
            $table->datetime('depart_at')->nullable();
            $table->enum("roomt_type",["single","twin"])->nullable();
            
            $table->integer('child1')->nullable();
            $table->integer('child2')->nullable();
            $table->integer('infant')->nullable();
            $table->integer('pax')->nullable();
            $table->string('tour_name');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('nationality')->nullable();
            $table->string('email')->nullable();
            $table->string('tel1')->nullable();
            $table->string('referral_code')->nullable();
            $table->string('remark')->nullable();

            $table->longText('device_info')->nullable();
            $table->longText('payload')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip');
    }
}
