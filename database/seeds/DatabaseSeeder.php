<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        $user = new User();
        $user->name = 'admin';
        $user->email = 'bizsuit.admin';
        $user->password = Hash::make('123456');
        $user->save();
        Schema::enableForeignKeyConstraints();
    }
}
