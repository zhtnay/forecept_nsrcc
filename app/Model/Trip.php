<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $table = "trip";
    protected $fillable = [
        'pax',
        'email',
        'first_name',
        'tour_name'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($trip)
        {
            
        });
    }
}
