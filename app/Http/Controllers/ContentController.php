<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmationEmail;
use App\Mail\RegistrationEmail;
use App\Mail\EnquiryAdminEmail;
use App\Mail\EnquiryClientEmail;
use App\Trip;

use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function HomePage(){
        $trips = collect($this->GetTripList())->all();
        $tripHighlight = collect($this->GetTripHighlight())->map(function($item, $key) use ($trips){
            $trip = collect($trips)->firstWhere('code',$item);
            $trip->previewDateTime = $trip->previewDate . " " . $trip->previewTime;
            return $trip;
        })
        ->groupBy('previewDateTime');
        $tripList = collect($this->GetTripList())->all();

        return view('home',compact('tripList','tripHighlight'));
    }

    public function GetTripDetail($code){
        $trip = collect($this->GetTripList())->where('code',$code)->first();
        if(empty($trip)) return redirect()->route('home');
        return view('detail',compact('trip'));
    }

    public function GetTripConfirm($code){
        $trip = collect($this->GetTripList())->where('code',$code)->first();
        if(empty($trip)) return redirect()->route('home');
        return view('confirm',compact('trip'));
    }

    public function PostTripConfirm(Request $request){
        $mailSuccess = 1;
        // insert into db
        $input = $request->all();
        $input['datetime'] = date('Y-m-d H:i:s');
        $trip = new Trip();
        $trip->first_name = $input['name'];
        $trip->email = $input['email'];
        $trip->pax = $input['pax'];
        $trip->tour_name = $input['tourname'];
        $trip->trip_code = $input['code'];
        $trip->payload = json_encode($input);
        // $trip->save();

        // send email function
        Mail::send(new RegistrationEmail($request->all()));

        if (Mail::failures()) {
            $mailSuccess = 0;
            $input['RegistrationMail'] = 'Send Failed';
        }

        Mail::send(new ConfirmationEmail($request->all()));

        if (Mail::failures()) {
            $mailSuccess = 0;
            $input['confirmationMail'] = 'Send Failed';
        }

        $directoryName = $this->resultDirectory();
        $filename = $input['email'].date('-Ymd-His').'.json';
        $file = $directoryName.'/'.$filename;
        file_put_contents($file, json_encode($input));

        if(!$mailSuccess){
            return json_encode(["status" => "fail"]);
        }

        $trip = collect($this->GetTripList())->where('code',$request->code)->first();
        if(empty($trip)) return redirect()->route('home');
        $request->session()->put('trip',$trip);
        return redirect('thankyou');
    }

    public function GetTripEnquiry($code){
        $trip = collect($this->GetTripList())->where('code',$code)->first();
        if(empty($trip)) return redirect()->route('home');
        return view('enquiry',compact('trip'));
    }

    public function PostTripEnquiry(Request $request){
        $mailSuccess = 1;
        // insert into db
        $input = $request->all();
        $input['datetime'] = date('Y-m-d H:i:s');
        $trip = new Trip();
        $trip->first_name = $input['name'];
        $trip->email = $input['email'];
        $trip->tour_name = $input['tourname'];
        $trip->trip_code = $input['code'];
        $trip->pax = "";
        $trip->payload = json_encode($input);
        // $trip->save();

        // send email function
        Mail::send(new EnquiryAdminEmail($request->all()));

        if (Mail::failures()) {
            $mailSuccess = 0;
            $input['RegistrationMail'] = 'Send Failed';
        }

        Mail::send(new EnquiryClientEmail($request->all()));

        if (Mail::failures()) {
            $mailSuccess = 0;
            $input['confirmationMail'] = 'Send Failed';
        }        

        $directoryName = $this->resultDirectory();
        $filename = $input['email'].date('-Ymd-His').'.json';
        $file = $directoryName.'/'.$filename;
        file_put_contents($file, json_encode($input));

        if(!$mailSuccess){
            return json_encode(["status" => "fail"]);
        }

        $trip = collect($this->GetTripList())->where('code',$request->code)->first();
        if(empty($trip)) return redirect()->route('home');
        $request->session()->put('trip',$trip);
        return redirect('thankyou');
    }

    public function GetTripList(){
        return  json_decode(file_get_contents(resource_path("data/trip.json")));
    }

    public function GetTripHighlight(){
        return  json_decode(file_get_contents(resource_path("data/trip_home.json")));
    }

    function resultDirectory(){
        $directoryName = storage_path('app/result/');
        if(!is_dir($directoryName)){
            mkdir($directoryName, 0777, true);
        }
        return $directoryName;
    }
}
