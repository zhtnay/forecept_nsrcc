<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;
use Validator;

class UserController extends Controller
{
   public function getPasswordUpdate()
   {
        return view('admin.password');
   }

   public function postPasswordUpdate(Request $request)
   {
      Validator::extend('current_password', function ($attribute, $value, $parameters, $validator) {
         $user = User::find($parameters[0]);
         return $user && Hash::check($value, $user->password);
     });

      $validator = $request->validate([
         'current_password' => 'required|current_password:'.Auth::user()->id,
         'new_password' => 'required|min:6|same:new_password',
         'confirm_password' => 'required|same:new_password',
      ]);

      $user = User::find(Auth::user()->id);
      $user->password = Hash::make($request['new_password']);
      $user->save();

      return redirect()
      ->route('admin.password.form')
      ->with('success','Password Update successfully.');
   }
}
