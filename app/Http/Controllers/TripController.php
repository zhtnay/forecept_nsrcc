<?php

namespace App\Http\Controllers;

use App\Trip;
use Illuminate\Http\Request;

class TripController extends Controller
{
    public function index()
    {
        return view('admin.trip');
    }
    
    public function datatables(Request $request)
    {
        $columns = [
            0 => 'tour_name',
            1 => 'first_name',
            2 => 'email',
            3 => 'pax',
        ];
        $start = $request->input ('start');
        $length = $request->input ('length');
        $data = Trip::Select(['id','tour_name','first_name','email','pax']);

        $totalData = $data->count();            //Total record
        $totalFiltered = $totalData;

        // keyword search
        if ($request->has ( 'search' )) {
            if ($request->input ( 'search.value' ) != '') {
                $keyword = $request->input ( 'search.value' );
                $data->where('tour_name','Like', '%' . $keyword . '%' )
                     ->orWhere('first_name','Like', '%' . $keyword . '%' )
                     ->orWhere('email','Like', '%' . $keyword . '%' )
                ;
            }
        }

        // sorting, orderby
        if ($request->has ( 'order' )) {
            if ($request->input ( 'order.0.column' ) != '') {
                $orderColumn = $request->input ( 'order.0.column' );
                $orderDirection = $request->input ( 'order.0.dir' );
                $data->orderBy ( $columns [intval ( $orderColumn )], $orderDirection );
            }
        }

        $totalFiltered = $data->count ();
        $data = $data->skip ( $start )->take ( $length )->get();

        return [
            "draw" => intval ( $request->input ( 'draw' ) ), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval ($totalData), // total number of records
            "recordsFiltered" => intval ($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data
        ];
    }
}
