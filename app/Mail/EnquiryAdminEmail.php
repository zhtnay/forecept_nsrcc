<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnquiryAdminEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject($this->data['email']." - New Tour Enquiry")
            ->to(['helen_tan@wholesale-partners.com','jessie_tan@wholesale-partners.com'])
            ->from('enquiry@wholesale-partners.com');
				return $this->view('mail.enquiry-admin')
                            ->with('data',$this->data);
    }
}
