<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		@yield('meta')
        
        <link rel="icon" href="{{ asset('/assets/img/logo.ico') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <!-- <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"> -->
        <style>

        </style>
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144580272-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-144580272-2');
        </script> -->

        <!-- Start of Zendesk Widget script -->
        <!-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=web_widget/wholesalepartners.zendesk.com"></script> -->
        <!-- End of Zendesk Widget script -->
    </head>
    <body>
        <div id="wrapper">
            @include('partial.header')
            <section class="content">
                @yield('content')
            </section>
            @include('partial.footer')
        </div>
        
        <script src="{{ asset ("/js/app.js") }}"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script>
            $(document).ready( function(){
                $("#overlay").fadeOut(500);
            });
        </script>
        @yield('script')
        @stack('script')
        <div id="overlay" class="overlay--active">
        </div>
    </body>
</html>
