@extends('layout')
@section('content')
<div id="wrapper" class="confirm">
    <section id="content">
        <div id="banner" class="block block-banner banner-confirm">
        </div>
        <div class="block block-form">
            <br />
            <br />
            <form id="TripForm" action="{!!route('enquiry.trip')!!}" method="post" accept-charset="UTF-8">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="">ENQUIRY FORM</h2>
                                <br />
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="">Tour Name</label>
                                        <textarea class="form-control" name="tourname" placeholder="" readOnly>{!!strip_tags($trip->name)!!}&#13;&#10;@if(!empty($trip->previewDate))Tour Preview Dates {!!strip_tags($trip->previewDate)!!} {!!strip_tags($trip->previewTime)!!}@endif</textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <i class="text-warning mr-1">*</i><label for="">Name</label>
                                        <input type="text" name="name" class="form-control" id="" placeholder="" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                    <i class="text-warning mr-1">*</i><label for="">Email</label>
                                        <input type="email" name="email" class="form-control" id="" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12">
                                    <label for="">Enquiry Message</label>
                                        <textarea class="form-control" name="remark" placeholder="" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="float-right">
                                    <input type="hidden" name="code" value="{{$trip->code}}">
                                    <a href="{{url('trip/'.$trip->code)}}" class="btn btn-secondary mr-3">Back</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <img src="{!!asset($trip->img[0])!!}" class="w-100 img-fluid d-block" onerror="this.onerror=null;this.src='/img/img-not-found.png';"/>
                                <br/>
                                <h5>{!!$trip->name!!}</h5>
                                <p>
                                {{$trip->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </section>
</div>
@endsection
@section('script')
<script>
    $(function () {
        $("#TripForm").parsley({
            errorClass: 'is-invalid text-danger',
            successClass: 'is-valid', // Comment this option if you don't want the field to become green when valid. Recommended in Google material design to prevent too many hints for user experience. Only report when a field is wrong.
            errorsWrapper: '<div class="text-danger"></div>',
            errorTemplate: '<span></span>',
            trigger: 'change'
        })
    })
</script>
@endsection
