@extends('layout')
@section('content')
<div id="wrapper" class="homepage">
    <section id="content">
        <div id="banner" class="block block-banner banner-main">
            <div class="banner-list slider-container">
                <div class="banner-item banner-1 slider-item" style="background:url({!!asset('/img/banner-1.jpg')!!}) center center / cover no-repeat;">
                    <div class="banner-content h-100 container">
                        <div class="row h-100 align-items-center">
                            <div class="col-12 col-md-9">
                                <div class="text-1-container">
                                    <div class="shade-box">                                        
                                    </div>
                                    <h1>2020 Holiday Deals by <span class="text-sm-nowrap">Wholesale Partners</span></h1>
                                    <h5>(Chan Brothers Group of Companies)</h5>
                                    <div>
                                        <h2 class="mt-5">Exclusively for NSRCC Members</h2>
                                        <h5 >15 December 2019 – 30 April 2020</h5>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>
                <div class="banner-item banner-2 slider-item" style="background:url({!!asset('/img/banner-2.jpg')!!}) center center / cover no-repeat;">
                    <div class="banner-content h-100 container">
                        <div class="row h-100 align-items-center">
                            <div class="col-12 col-md-9">
                                <div class="text-1-container">
                                    <div class="shade-box">                                        
                                    </div>
                                    <h1>2020 Holiday Deals by <span class="text-sm-nowrap">Wholesale Partners</span></h1>
                                    <h5>(Chan Brothers Group of Companies)</h5>
                                    <div>
                                        <h2 class="mt-5">Exclusively for NSRCC Members</h2>
                                        <h5 >15 December 2019 – 30 April 2020</h5>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>
                <div class="banner-item banner-3 slider-item" style="background:url({!!asset('/img/banner-3.jpg')!!}) center center / cover no-repeat;">
                    <div class="banner-content h-100 container">
                        <div class="row h-100 align-items-center">
                            <div class="col-12 col-md-9">
                                <div class="text-1-container">
                                    <div class="shade-box">                                        
                                    </div>
                                    <h1>2020 Holiday Deals by <span class="text-sm-nowrap">Wholesale Partners</span></h1>
                                    <h5>(Chan Brothers Group of Companies)</h5>
                                    <div>
                                        <h2 class="mt-5">Exclusively for NSRCC Members</h2>
                                        <h5 >15 December 2019 – 30 April 2020</h5>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="block block-content">
            <div class="container" style="position:relative;">
                <div class="py-5">
                    <br />
                    <ul>
                        <li><h5>Redeem <b>Free 24” Crossing luggage*</b> with minimum spend of S$3,800 (Payment with UOB Cards only, while stocks last)</h5></li>
                        <li><h5>Receive <b>Free SATS Premier Lounge Access</b> per traveler with minimum spend of S$3,800 (Not applicable for Dream Cruise Package)</h5> </li>
                        <li><h5>Receive <b>Free Comfort Delgro Taxi Voucher worth $20</b> (Not applicable for Dream Cruise Package)</h5></li>
                    </ul>
                    <small class="pl-4">*Terms and Conditions Apply</small>
                    <div class="float-right"><a href="http://nsrcc.ttmktplc.com/" target="blank" class="btn btn-primary">Click here for Hotels</a></div>
                </div>
            </div>
            <!--<div class="container py-4">
                <div class="row">
                    <div class="col">
                        <br/>
                        <div class="d-none d-sm-block">
                            <table class="table table-bordered">
                                <colgroup>
                                    <col />
                                    <col />
                                    <col />
                                </colgroup>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Tour Preview Date & Time</th>
                                        <th>Holiday Tour Preview</th>
                                        <th>Click to register</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>18 January 2020 Saturday, 1.30pm</td>
                                        <td class="align-middle">Genting Dream<br /><br />Singapore - Ko Samui - Laem Chabang – Singapore</td>
                                        <td class="align-middle p-1 text-center"><a href="https://www.eventbrite.sg/e/82133416259" class="btn btn-primary">Click</a></td>
                                    </tr>
                                    <tr>
                                        <td>18 January 2020 Saturday, 3pm</td>
                                        <td class="align-middle">Sakura Series<br /><br /></td>
                                        <td class="align-middle p-1 text-center"><a href="https://www.eventbrite.sg/e/82133881651" class="btn btn-primary">Click</a></td>
                                    </tr>
                                    <tr>
                                        <td>18 January 2020 Saturday, 4.30pm</td>
                                        <td class="align-middle">English Premier League<br /><br /></td>
                                        <td class="align-middle p-1 text-center"><a href="https://www.eventbrite.sg/e/82134094287" class="btn btn-primary">Click</a></td>
                                    </tr>
                                    @foreach($tripHighlight as $date => $trips)
                                        @if(count($trips) > 1)
                                            @foreach($trips as $index => $trip)
                                                @if($index == 0)
                                                    <tr>
                                                        <td rowspan="{!!count($trips)!!}">{!!$date!!}</td>
                                                        <td class="align-middle">{!!$trip->name!!}</td>
                                                        <td class="align-middle p-1 text-center"><a href="/confirm/{!!$trip->code!!}" class="btn btn-primary">Click</a></td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td class="align-middle">{!!$trip->name!!}</td>
                                                        <td class="align-middle p-1 text-center"><a href="/confirm/{!!$trip->code!!}" class="btn btn-primary">Click</a></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @elseif(count($trips) == 1)
                                            <tr>
                                                <td>{!!$date!!}</td>
                                                <td class="align-middle">{!!$trips[0]->name!!}</td>
                                                <td class="align-middle p-1 text-center"><a href="/confirm/{!!$trips[0]->code!!}" class="btn btn-primary">Click</a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="d-block d-sm-none">
                            <ul class="list-group">
                                <li class="list-group-item bg-dark text-white">
                                    Holiday Tour Preview 
                                </li>
                                @foreach($tripHighlight as $date => $trips)
                                    @foreach($trips as $index => $trip)
                                        
                                        <li class="list-group-item">
                                            <h5>{!!$date!!}</h5>
                                            <p>{!!$trip->name!!}</p>
                                            <a href="/confirm/{!!$trip->code!!}" class="btn btn-primary">Click to register</a>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="block block-list">
            <div class="container py-5"> 
                <div class="row">
                @foreach($tripList as $trip)
                    <div class="col-lg-4 col-md-6 col-12 pb-5">
                        <div class="card">
                        <a class="btn btn-outline" href="{{!empty($trip->directLink) ? $trip->directLink : url('trip/'.$trip->code)}}" @if(!empty($trip->directLink)) target="_blank" @endif ><div class="trip-img" style="background-image:url({!!asset($trip->img[0])!!}),url({!!asset('/img/img-not-found.png')!!});"></div></a>
                            <div class="card-body">
                                <div class="trip-desc">
                                    <h5 class="card-title font-weight-bold">{!!$trip->name!!}</h5>
                                    @if(!empty($trip->previewDate))<p class="card-text">Preview Date:<br>{{$trip->previewDate}} {{$trip->previewTime}}</p>@endif
                                </div>
                                <a class="btn btn-primary text-white mt-4" href="{{!empty($trip->directLink) ? $trip->directLink : url('trip/'.$trip->code)}}">View</a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('script')
<script>
$(function(){
	$(".banner-list").slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay:true,
	autoplaySpeed:5000,
	arrows: false,
	fade: true,
	dots:true
	});
});
</script>
@endsection