@extends('layout')
@section('content')
<div id="wrapper" class="thankyou">
    <section id="content">
        <div id="banner" class="block block-banner banner-container banner-sub">
            <div class="banner-list">
                <div class="banner-item banner-1"
                    style="background:url({{session('trip')->img[0]}}) center center / cover no-repeat;">

                </div>
            </div>
        </div>
        <div class="block block-content container pb-5 text-center">
            <h1 class="py-4">Thank you for registering for the Tour Preview.</h1>
            <h4>For any urgent enquiries about our tours, please contact us at <a href="mailto:enquiry@wholesale-partners.com">enquiry@wholesale-partners.com</a></h4>
        </div>
    </section>    
</div>
@endsection
@section('script')
@endsection
