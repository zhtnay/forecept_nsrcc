<header>
    <div class="header-info text-center py-3">
        <div class="header-logo">
            <a href="{{ route('home') }}">
                <img src="{{ asset('/img/logo.png') }}" class="img-fluid"/>
                <img src="{{ asset('/img/nsrcc-logo.jpg') }}" class="nsrcc-logo img-fluid"/>
            </a>
        </div>
    </div>
</header>