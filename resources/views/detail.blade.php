@extends('layout')
@section('content')
<div id="wrapper" class="detail">
    <section id="content">
        <div id="banner" class="block block-banner banner-container banner-sub">
            <div class="banner-list">
                <div class="banner-item banner-1"
                    style="background:url({!!asset($trip->img[0])!!}) @if(!empty($trip->imgPosition->detailBanner)) {{$trip->imgPosition->detailBanner}} @else center center @endif / cover no-repeat,url({!!asset('/img/img-not-found.png')!!};">

                </div>
            </div>
        </div>
        <div class="block block-content">
            <div class="container pt-5">
                <div class="row pb-3">
                    <h1 class="trip-title col-lg-8">{!!$trip->name!!}</h1>
                    <h1 class="col-lg-4"><span class="badge badge-primary float-right">{!!$trip->price!!}</span></h1>
                </div>
            </div>
            <div class="trip-main">
                <div class="container">
                    <div class="row pt-4">
                        @if(!empty($trip->previewDate))
                        <div class="col-lg-6 pb-3 pr-5">
                            <h4>Tour Preview</h4>
                            <p>Date: {!!$trip->previewDate!!}</p>
                            <p>Time: {!!$trip->previewTime!!}</p>
                            <p>Venue: {!!$trip->previewVenue!!}</p>
                        </div>
                        @endif
                        <div class="col-lg-6 pb-3 pr-5">
                            <div class="pb-3">
                                <h4>Sales Period</h4>
                                <p>{!!$trip->salesPeriod!!}</p>
                            </div>
                            <div class="pb-3">
                                <h4>Travel Period</h4>
                                <p>{!!$trip->travelPeriod!!}</p>
                            </div>
                        </div>
                    </div>
                    @if(!empty($trip->brochureLink))<a target="_blank" href="{!!$trip->brochureLink!!}" class="btn btn-warning text-white mb-4 font-weight-bold">Get Brochure</a>
                    @else For booking or enquiry, please email to <a style="text-decoration:underline;" href="mailto:enquiry@wholesale-partners.com">enquiry@wholesale-partners.com</a> or click on “Submit Enquiry” button on the right side.<br /><br />
                    @endif
                </div>
            </div>
            <div class="trip-detail py-4">
                <div class="container">
                    <h4><u>Package Details</u></h4><br>
                    <div class="row">
                        <div class="col-lg-6 col-12">
                        @foreach($trip->packageDetail as $packageDetail)
                            {!!$packageDetail!!}<br>
                        @endforeach
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="img-slick slider-container">
                                @foreach($trip->img as $img)
                                <div class="slick-item slider-item">
                                    <div class="h-100" style="background:url({!!asset($img)!!}) center center / cover no-repeat,url({!!asset('/img/img-not-found.png')!!};"></div>
                                </div>

                                @endforeach
                            </div>
                            <p class="table table-bordered p-5">{!!$trip->description!!}</p>
                            <h4 class="mt-4">Terms & Condition:</h4>
                            <ul class="list-inline">
                            @foreach($trip->tnc as $tnc)
                            <li style="word-break:break-word;">{!!$tnc!!}</li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                    <i>For our company profile, please visit us at <a target="_blank" href="https://www.wholesale-partners.com">www.wholesale-partners.com</a></i>
                </div>
            </div>
        </div>
    </section>
    <div class="fab-float fab-wrapper">
        <div class="fab-float fab-back">
            <a href="{{route('home')}}" class="btn btn-secondary border btn-lg font-weight-bold">Back</a>
        </div>
        <div class="fab-float fab-booknow">
            <a href="@if(!empty($trip->previewLink)){{ $trip->previewLink }}@elseif(!empty($trip->previewDate)){{ url('confirm/'.$trip->code) }}@else{{ url('enquiry/'.$trip->code)}}@endif" class="btn btn-primary btn-lg text-white font-weight-bold">
                @if(!empty($trip->previewDate))
                    SignUp for Preview Now!
                @else
                    Submit Enquiry
                @endif
            </a>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(function(){
	$(".img-slick").slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay:true,
	autoplaySpeed:5000,
	arrows: false,
	fade: true,
	dots:true
	});
});
</script>
@endsection
