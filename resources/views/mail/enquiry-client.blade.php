<p>Dear {!!$data['name']!!}</p>
<h1>Thank you for enquiry for the Tour.</h1><br>
<h4>For any urgent enquiries about our tours, please contact us at enquiry@wholesale-partners.com</h4><br>
<br>
<p>Best regards,</p>
<p><b>Wholesale Partners Pte Ltd</b></p>
<p>(Chan Brothers Group of Companies)</p>
<p>150 South Bridge Road #05-04 Fook Hai Building Singapore 058727</p>
<p>Tel: +65 6212 9548 / 6212 9549</p>
