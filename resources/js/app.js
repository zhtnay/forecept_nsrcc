window.$ = window.jQuery = require('jquery');
window.moment = require('moment');
window.moment.locale('en-gb');

require('bootstrap');
require('parsleyjs');