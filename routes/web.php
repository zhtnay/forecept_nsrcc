<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [ 'as' => 'home' ,function () {
//     return view('home');
// }]);
Route::get('/','ContentController@HomePage')->name('home');
Route::get('/list','ContentController@GetTripList');
Route::get('/trip/{code}','ContentController@GetTripDetail');
Route::get('/confirm/{code}','ContentController@GetTripConfirm');
Route::post('/confirm/trip','ContentController@PostTripConfirm')->name('confirm.trip');
Route::get('/enquiry/{code}','ContentController@GetTripEnquiry');
Route::post('/enquiry/trip','ContentController@PostTripEnquiry')->name('enquiry.trip');
Route::get('/thankyou',function(){
    return view('thankyou');
})->name('thankyou');


Route::get('/login', 'Auth\LoginController@index')->name('login'); // for naming
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/login', 'Auth\LoginController@index');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('/password-update','UserController@getPasswordUpdate')->name('password.form');
    Route::post('/password-update','UserController@postPasswordUpdate')->name('password.update');

    Route::group(['middleware' => ['auth']], function ()
    {
        Route::get('/dashboard', function(){
            return view('admin.dashboard');
        });
        Route::get('/trip','TripController@index')->name('trip.index');
        Route::get('/trip/list','TripController@datatables')->name('trip.list');
    });
});
